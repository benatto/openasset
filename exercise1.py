import sys
import subprocess

"""
Considerations:
    - I tried to make it simple and fast
    - It would be better to use argparse for command line tools
    - It would be better writting tests
"""


# magick convert image.jpg -resize 50% output.png
def convert(path: str, scale: str, output):
    subprocess.run(["convert", path, "-resize", f"{scale}%", output])


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: exercise1.py <IMAGE_PATH> <SCALE> <IMAGE_OUTPUT>")
        print("Example: exercise1.py image.jpg 50 new.jpg")
        sys.exit(1)

    convert(sys.argv[1], sys.argv[2], sys.argv[3])
