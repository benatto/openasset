## Exercise 3

Imagine we are to turn the script in Section A into a cloud service. You can assume that the
original script has been expanded into a feature-complete wrapper for ImageMagick. Describe
how you would scale this into a service that is capable of handling thousands of requests per
minute.


### Infrastructure: which hosting platform and specific services would you use (feel free to draw any diagrams to aid in the description of your architecture choices)?

- You could use barematel solution and deploy all services necessary on top a linux server, for example
brokers/queues, databases and so on. The engineers will have to maintain the infrastructure, might not be ideal for most of companies.

- If you decide to use a cloud-infrastructure like AWS and GCP, all the infrastructure will be provided for you.

```
 +----------+     +-----------------------+     +----------------------+     +---------------------+      +----------------+
 |          |     |                       |     |                      |     |                     |      |                |
 | Requests | --> | Image Request (Queue) | --> | Consumers/Processors | --> | Store Image (Queue) | -->  | Store Services |
 |          |     |                       |     |                      |     |                     |      |                |
 +----------+     +-----------------------+     +----------------------+     +---------------------+      +----------------+

* You could have a pipeline to process all requests 
* All the requests are injected in a Queue (SQS)
* Depending of the number of requests, one or N services can consume from the "Image Request Queue" and process the message/image (Consumers/Processors).
* The Consumers/Processors after process the message/image can publish a message to "Store Image (Queue)"
* Depending of the number of requests in the Queue, one or N services can consume and store images in S3 for example.
```

### Implementation: deployment pipeline, testing, on-going service maintenance, availability and redundancy.

* Usually I use Gitlab as CI/CD and in my pipeline runs: build, unit and integration tests and deploy to cloud.
* I do like the idea to services be able to run in local machine as well, there are some efforts for different cloud providers,
like localstack and skaffold.
* Availability
* You could use sentry to monitor and diagnose the platform.
* You could use fresh ping for availability and some analytics reports.
* I think there are a lot of tools out there to be used.

### Observability: logging, monitoring and alarming.

* I think I answered in the question above as well
* Sentry
* Cloudwatch
* Freshping
* I worked with PagerDutty before as well

### Variable load: how would you design the service for spiky / smooth / constant demand?

* I'm not really sure if I understood the question :). We could use elastic beanstalk to manage that for us.
I honestly never worked with beanstalk before, but I hear a lot about it.

### Data persistence: how would you persist results and image data?

Depends what we would like to persist, but for this problem we could persist image and image information (JSON if necessary)
into S3. If necessary you could make use of AWS RDS depending what is the nature of data we want to store.