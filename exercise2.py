import json
import sys
import subprocess

from typing import List


"""
Considerations:
    - I tried to make as simple and fast as possible
    - It would be better to use argparse (or any other command line tool like, click for example) for command line tools
    - It would be better writting tests ;)
"""

# convert image.jpg image.png
def jpg2png(path: str, output: str):
    subprocess.run(["convert", path, output])


# convert image.jpg -resize 50% output.png
def convert(path: str, scale: str, output) -> None:
    subprocess.run(["convert", path, "-resize", f"{scale}%", output])


def convert_all(images: List) -> None:
    for image in images:
        convert(image.get("path"), image.get("scale"), image.get("output"))


def load_json(filename: str) -> List:
    with open(filename) as f:
        return json.load(f)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: exercise2.py <JSON_FILE>")
        print("Example: exercise2.py images.json 50")
        sys.exit(1)

    images = load_json(sys.argv[1])
    convert_all(images)
